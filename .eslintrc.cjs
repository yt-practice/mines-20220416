module.exports = {
	root: true,
	parser: '@typescript-eslint/parser',
	parserOptions: {
		project: './tsconfig.json',
		sourceType: 'module',
	},
	settings: {
		'import/resolver': { typescript: { alwaysTryTypes: true } },
		'svelte3/typescript': () => require('typescript'),
	},
	env: {
		node: true,
		jest: true,
	},
	plugins: ['svelte3', '@typescript-eslint', 'import'],
	extends: ['@herp-inc', 'prettier'],
	rules: {
		yoda: ['error', 'always', { onlyEquality: true, exceptRange: true }],
		complexity: ['error', 20],
		'prefer-arrow-callback': 'error',
		'arrow-body-style': ['error', 'as-needed'],
		'no-console': false,
		'no-negated-condition': false,
		'import/no-default-export': false,
		'@typescript-eslint/array-type': ['error', { default: 'array' }],
		'@typescript-eslint/explicit-function-return-type': false,
		'@typescript-eslint/explicit-module-boundary-types': false,
		'@typescript-eslint/no-var-requires': false,
		'@typescript-eslint/strict-boolean-expressions': false,
		'@typescript-eslint/prefer-nullish-coalescing': false,
	},
	overrides: [
		{ files: ['*.svelte'], processor: 'svelte3/svelte3' },
		{
			files: ['*.ts', '*.tsx'],
			rules: { '@typescript-eslint/no-var-requires': 'error' },
		},
	],
}
